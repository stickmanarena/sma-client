screen = {
    ctx : null,        
    zoom : 2.8, //params.screen.initZoom,
    width: 0,
    height: 0,
    map: null,    
    stepCount: 0,
    init(map, width, height, cameraX, cameraY, ctx){                        
        screen.ctx = ctx;        
        screen.map = map;
        screen.cameraX = cameraX - screen.width/2*(1/screen.zoom);
        screen.cameraY = cameraY - screen.height/2*(1/screen.zoom);
        screen.width = width;
        screen.height = height;
        screen.clear();
        screen.drawMap();
        draw.ctx = screen.ctx;    
    },
    drawStep(allCharacters, cameraX, cameraY, sightRange){
        screen.stepCount++;
        screen.clear();     
        screen.drawMap();        
        screen.drawCharacters(allCharacters);                    
    },
    drawCharacters(allCharacters){             
        if (screen.ctx == null) return;        
        for (let i=0; i<allCharacters.length; i++){
            let ch = allCharacters[i];        
            let animation = animations[ch.state];           
            let frameSeq = (screen.stepCount - 0 /*npc.lastStateChange*/) % animation.length;
            let body = animation[frameSeq];              
            draw.color = ch.color;
            draw.drawBody(body, (ch.x+ch.offsetX-screen.cameraX)*screen.zoom, (ch.y+ch.offsetY-screen.cameraY)*screen.zoom);
            draw.drawHealthBar(ch.life, ch.maxLife, (ch.x+ch.offsetX-screen.cameraX)*screen.zoom, (ch.y+ch.offsetY-screen.cameraY)*screen.zoom);
            
            // if this is me: camera follows
            if (ch.id == player.id) {
                screen.cameraX = ch.x - screen.width/2/screen.zoom;
                screen.cameraY = ch.y - screen.height/2/screen.zoom;
            }
        }
    },
    drawMap(){
        if (screen.map == null) return;
        draw.drawMap(screen.map, 23*screen.zoom, screen.cameraX*screen.zoom, screen.cameraY*screen.zoom);
    },
    clear(){
        if (screen.ctx == null) return;
        draw.clear(screen.width, screen.height);
    }
    
}