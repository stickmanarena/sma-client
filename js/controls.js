controls = {    
    init(canvasJQ){                
        $(window).bind('mousewheel', function(e){
            if(e.originalEvent.wheelDelta /120 > 0) screen.zoom *= 1.1;
            else screen.zoom /= 1.1;                                            
        });
        window.addEventListener('keydown', controls.keyDown, false);
        window.addEventListener('keyup', controls.keyUp, false);        
   },
    
    
    keyDown : (e) => {                     
        console.log("keyup " + e.keyCode);                                          
        wsif.sendKeyDown(e.keyCode);                        
    },
    
    keyUp : (e) => {                        
        console.log("keydown " + e.keyCode)
        wsif.sendKeyUp(e.keyCode);
    },
}