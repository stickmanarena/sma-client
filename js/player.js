player = {    
    id: 0,                          // if anonymus, then a timestamp
    x : 100,
    y : 200,
    sightRange: 600,                // px
    init(p){  
        player.id = p.id;
        player.x = p.x;
        player.y = p.y;
        player.color = p.color;
        player.state = p.state;
    },    
}